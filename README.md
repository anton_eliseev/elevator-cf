# Start application
```
git clone https://anton_eliseev@bitbucket.org/anton_eliseev/elevator-cf.git
cd elevator-cf
mvn exec:java "-Dexec.args=20 7 2 3"
```
You can provide your own arguments:
- first: floors count (20 in example)
- second: one floor height in meters (7 in example)
- third: elevator speed in meters/sec (2 in example)
- fourth: doors opened time in seconds (3 in example)

# Elevator interaction
To simulate actions user have to insert values in console.
```
i2<Enter>
```
for pushing button with certain floor inside elevator (here it is 2)
```
o3<Enter>
```
for pushing call elevator button from stairwell on certain floor (here it is 3)