package cf;

import java.util.Observable;
import java.util.Observer;

public class ElevatorDisplay implements Observer {

    private double elevatorFloor;
    private boolean doorsClosed;
    private MovingDirection movingDirection;

    public ElevatorDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ElevatorState) {
            ElevatorState elevatorState = (ElevatorState) o;
            this.elevatorFloor = elevatorState.getCurrentFloor();
            this.doorsClosed = elevatorState.getDoorsClosed();
            this.movingDirection = elevatorState.getMovingDirection();
            displayElevatorState();
        }
    }

    private void displayElevatorState() {
        System.out.println(
            String.format("Elevator is on %d floor. %s. Doors are %s",
                Math.round(elevatorFloor),
                movingDirection == MovingDirection.STANDING ? "Standing" : "Moving " + movingDirection.name(),
                doorsClosed ? "closed" : "opened, people loading/unloading."
            )
        );
    }
}
