package cf;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AppStart {
    public static void main(String[] args) {

        try {
            int floorCount = Integer.valueOf(args[0]);
            double floorHeight = Integer.valueOf(args[1]);
            double spd = Double.valueOf(args[2]);
            double doorsOpenTime = Double.valueOf(args[3]);
            Elevator elevator = new Elevator(floorCount, floorHeight, spd, doorsOpenTime);
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(elevator);

            while (true) {
                Scanner inputScanner = new Scanner(System.in);
                String inputString = inputScanner.nextLine();
                if (inputString != null && inputString.length() > 1) {
                    try {
                        if (inputString.toLowerCase().startsWith("o")) {
                            elevator.pushButtonOutside(parseFloor(inputString));
                        } else if (inputString.toLowerCase().startsWith("i")) {
                            elevator.pushButtonInside(parseFloor(inputString));
                        }
                    }
                    catch (NumberFormatException exc) {
                        System.out.println("Bad floor.");
                    }
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException|NumberFormatException exc) {
            throw new RuntimeException("Wrong arguments.");
        }
    }

    public static int parseFloor(String inputString) {
        return Integer.valueOf(inputString.substring(1));
    }
}
