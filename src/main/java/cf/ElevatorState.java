package cf;

import java.util.Observable;

public class ElevatorState extends Observable {

    private volatile double currentFloor = 1;
    private boolean doorsClosed = true;
    private MovingDirection movingDirection = MovingDirection.STANDING;

    public ElevatorState() {
    }

    public double getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(double currentFloor) {

        double prev = movingDirection == MovingDirection.UP ? Math.floor(this.currentFloor) : Math.ceil(this.currentFloor);
        double next = movingDirection == MovingDirection.UP ? Math.floor(currentFloor) : Math.ceil(currentFloor);

        this.currentFloor = currentFloor;

        if (prev != next) {
            stateChanged();
        }
    }

    public boolean getDoorsClosed() {
        return doorsClosed;
    }

    public void setDoorsClosed(boolean doorsClosed) {
        this.doorsClosed = doorsClosed;
        stateChanged();
    }

    public MovingDirection getMovingDirection() {
        return movingDirection;
    }

    public void setMovingDirection(MovingDirection movingDirection) {
        MovingDirection prev = this.movingDirection;
        MovingDirection next = movingDirection;

        this.movingDirection = movingDirection;

        if (prev != next) {
            stateChanged();
        }
    }

    public void stateChanged() {
        setChanged();
        notifyObservers();
    }
}
