package cf;

import java.util.*;
import java.util.concurrent.*;

public class Elevator implements Runnable {

    private static final int MILLIS_IN_SEC = 1000;
    private static final int TIME_SLOT = 100;

    private ElevatorState state;
    private ElevatorDisplay display;
    private NavigableSet<Double> insideQueue = new ConcurrentSkipListSet<>((o1, o2) -> Double.compare(o1, o2));
    private Queue<Double> outsideQueue = new UniqueLinkedBlockingQueue<>();

    /* speed in floors/TIME_SLOT */
    private double speed;

    private int doorsOpenedTime;

    private int minFloor = 1;
    private int maxFloor;

    public Elevator(int floorsCount, double floorHeight, double speed, double doorsOpenedTime) {
        state = new ElevatorState();
        this.doorsOpenedTime = (int) Math.round(doorsOpenedTime * MILLIS_IN_SEC);
        this.maxFloor = floorsCount;
        this.speed = speed / floorHeight / MILLIS_IN_SEC * TIME_SLOT;
        display = new ElevatorDisplay(state);
    }

    @Override
    public void run() {
        System.out.println("Elevator put in operation.");
        System.out.println(String.format(
            "Initial state: currentFloor: %d, floorsCount: %d, speed: %s, doorsClosed: %b, doorsOpenedTime: %s ",
            Math.round(state.getCurrentFloor()),
            maxFloor,
            (speed * MILLIS_IN_SEC / TIME_SLOT) + " floors/s",
            state.getDoorsClosed(),
            (doorsOpenedTime / MILLIS_IN_SEC) + " s"
        ));

        while (true) {

            if (state.getDoorsClosed()) {

                Double currentFloor = state.getCurrentFloor();
                Double destinationFloor = peekNextDestinationFloor(currentFloor);

                if (destinationFloor > 0) {

                    if (destinationFloor > currentFloor) {
                        state.setMovingDirection(MovingDirection.UP);
                        Double next = state.getCurrentFloor() + speed;
                        if (next > destinationFloor) {
                            next = destinationFloor;
                        }
                        state.setCurrentFloor(next);
                    }
                    else if (destinationFloor < currentFloor) {
                        state.setMovingDirection(MovingDirection.DOWN);
                        Double next = state.getCurrentFloor() - speed;
                        if (next < destinationFloor) {
                            next = destinationFloor;
                        }
                        state.setCurrentFloor(next);
                    }

                    if (state.getCurrentFloor() == destinationFloor) {

                        if (state.getMovingDirection() != MovingDirection.STANDING || Objects.equals(outsideQueue.peek(), destinationFloor)) {

                            // elevator arrived at destination OR someone pressed outside button on elevator floor
                            stopAndOpenDoors();

                        }
                        removeFloorFromQueues(state.getCurrentFloor());
                    }
                }
            }

            try {
                Thread.sleep(TIME_SLOT);
            }
            catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public void pushButtonOutside(double floor) {
        if (floor <= maxFloor && floor >= minFloor) {
            outsideQueue.offer(floor);
        }
        else {
            System.out.println("Bad floor.");
        }
    }

    public void pushButtonInside(double floor) {
        if (floor <= maxFloor && floor >= minFloor) {
            insideQueue.add(floor);
        }
        else {
            System.out.println("Bad floor.");
        }
    }

    private Double peekNextDestinationFloor(Double currentFloor) {

        Double result = 0.0;

        if (!insideQueue.isEmpty()) {

            Double low = insideQueue.lower(currentFloor);
            Double high = insideQueue.higher(currentFloor);

            Double diffLow = low != null ? currentFloor - low : Integer.MAX_VALUE;
            Double diffHigh = high != null ? high - currentFloor : Integer.MAX_VALUE;

            if (low != null && high != null) {
                result = diffHigh >= diffLow ? low : high;
            }
            else {
                result = insideQueue.first();
            }
        }
        else if (!outsideQueue.isEmpty()) {
            result = outsideQueue.peek();
        }

        return result;
    }

    private void removeFloorFromQueues(Double currentFloor) {

        insideQueue.remove(currentFloor);
        outsideQueue.remove(currentFloor);
    }

    private void stopAndOpenDoors() {
        state.setMovingDirection(MovingDirection.STANDING);
        state.setDoorsClosed(false);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                Thread.sleep(doorsOpenedTime);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            state.setDoorsClosed(true);
        });
    }
}
