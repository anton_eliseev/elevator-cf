package cf;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class UniqueLinkedBlockingQueue<E> extends LinkedBlockingQueue<E> {

    private Set<E> set = ConcurrentHashMap.newKeySet();

    @Override
    public synchronized void put(E e) throws InterruptedException {
        if (set.contains(e)) {
            return;
        }
        set.add(e);
        super.put(e);
    }

    @Override
    public synchronized boolean offer(E e) {
        if (set.contains(e)) {
            return false;
        }
        set.add(e);
        return super.offer(e);
    }

    @Override
    public E take() throws InterruptedException {
        E head = super.take();
        set.remove(head);
        return head;
    }

    @Override
    public boolean remove(Object o) {
        set.remove(o);
        return super.remove(o);
    }

}
